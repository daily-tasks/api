module.exports = (async () => {
	require('./utils/errors');

	const config = await require('./utils/configuration')();

	return await require('./services')(config);
})().catch((error) => {
	console.error('Fatal Error', error);
});
