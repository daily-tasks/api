const _ = require('lodash');
const express = require('express');

const { errorHandler, registerRoutes, registerHandlers } = require('../utils/express');

module.exports = (config, services) => {
	const app = express();

	_.each(require('./lib')(config, services), (handler) => app.use(handler));

	registerHandlers(app, require('./middlewares')(config, services));

	app.use('/api', registerRoutes(express.Router(), require('./routes')(config, services).var));

	registerHandlers(app, require('./afterwares')(config, services));

	app.use(errorHandler);

	return app.listen(config.server.port, () => {
		console.log('Server ready');
	});
};
