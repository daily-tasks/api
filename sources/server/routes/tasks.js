module.exports = (config, { controllers: { tasks } }) => {
	return [ {
		method: 'get',
		path: '/',
		handler: tasks.list,
	}, {
		method: 'get',
		path: '/:id',
		contexter: (req) => req.params.id,
		handler: tasks.get,
	} ];
};
