module.exports = (config, { controllers: { households } }) => {
	return [ {
		method: 'get',
		path: '/',
		handler: households.list,
	}, {
		method: 'get',
		path: '/:id',
		contexter: (req) => req.params.id,
		handler: households.get,
	}, {
		method: 'get',
		path: '/:id/assignments',
		contexter: (req) => req.params.id,
		handler: households.assignments,
	} ];
};
