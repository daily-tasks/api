const fs = require('fs');
const _ = require('lodash');

module.exports = (config, services) => {
	return _([ ...fs.readdirSync(__dirname) ])
		.filter((fileName) => fileName !== 'index.js' && fileName.endsWith('.js'))
		.map((fileName) => ({
			path: `/${fileName.slice(0, -3)}`,
			routes: require(`./${fileName}`)(config, services),
		})).value();
};
