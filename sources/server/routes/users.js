const _ = require('lodash');

module.exports = (config, { controllers: { users } }) => {
	return [ {
		method: 'get',
		path: '/',
		handler: users.list,
	}, {
		method: 'get',
		path: '/:id',
		contexter: (req) => req.params.id,
		handler: users.get,
	}, {
		method: 'post',
		path: '/',
		contexter: (req) => [ _.pick(req.body, [ 'label', 'power' ]), _.get(req.body, 'householdId') ],
		handler: _.spread(users.create),
	} ];
};
