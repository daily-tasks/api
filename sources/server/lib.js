const bodyParser = require('body-parser');
const morgan = require('morgan');
const { DateTime } = require('luxon');

morgan.token('date', () => DateTime.utc().toISO());
morgan.format('dated', '[:date] :method :url :status len::res[content-length] :response-time ms');

module.exports = () => {
	return [
		bodyParser.urlencoded({ extended: true }),
		bodyParser.json(),
		morgan('dated'),
	];
};
