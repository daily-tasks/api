const { throwIf } = require('@plokkke/toolbox');
const Errors = require('../../utils/errors');

module.exports = () => {
	return [
		async (req, res) => {
			throwIf(!res.headersSent, () => Errors.doesntExist({ name: req.originalUrl }));
		},
	];
};
