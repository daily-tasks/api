const { pipe } = require('../../utils/lodash');
const { unsequelize } = require('../../utils/sequelize');

module.exports = (config, { database: { models: { Task } } }) => {
	function list() {
		return Task.findAll();
	}

	function get(id) {
		return Task.findOne({ where: { id } });
	}

	function create(label, householdId) {
		return Task.create({
			label,
			householdId,
		});
	}

	return {
		create: pipe(create, unsequelize),
		get: pipe(get, unsequelize),
		list: pipe(list, unsequelize),
	};
};
