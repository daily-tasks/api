const _ = require('lodash');
const { DateTime } = require('luxon');
const { Op } = require('sequelize');

const { pipe } = require('../../utils/lodash');
const { unsequelize } = require('../../utils/sequelize');

module.exports = (config, { database: { models: { Household, User, Work } } }) => {
	const defaultInclude = [ 'inhabitants', 'tasks' ];

	async function list() {
		return Household.findAll({ include: defaultInclude });
	}

	async function get(id) {
		return Household.findOne({ where: { id }, include: defaultInclude });
	}

	async function create(label) {
		return Household.create({ label });
	}

	async function edit(id, label) {
		const household = await get(id);
		household.label = label;
		return household.save();
	}

	// TODO Algo
	async function assignments(id) {
		const household = await get(id);
		const now = DateTime.now();

		await Promise.all(_.map(household.inhabitants, async (inhabitant) => {
			const user = await User.findOne({
				where: { id: inhabitant.id },
				include: [ 'possibleTasks', 'possibleRewards' ],
			});
			console.log(inhabitant, user);
			let work = await Work.findOne({ where: {
				userId: user.id,
				createdAt: {
					[Op.between]: [ now.startOf('day'), now.endOf('day') ],
				},
			} });
			if (!work) {
				const task = _.sample(user.possibleTasks);
				const reward = _.sample(user.possibleRewards) || null;
				work = await Work.create({ userId: user.id, taskId: task.id, rewardId: reward && reward.id });
			}
			return { user, work };
		}));
	}

	return {
		assignments,
		create: pipe(create, unsequelize),
		edit: pipe(edit, unsequelize),
		get: pipe(get, unsequelize),
		list: pipe(list, unsequelize),
	};
};
