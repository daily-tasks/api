const fs = require('fs');
const _ = require('lodash');

module.exports = (config, services) => {
	return _([ ...fs.readdirSync(__dirname) ])
		.filter((fileName) => fileName !== 'index.js' && fileName.endsWith('.js'))
		.map((fileName) => [ fileName.slice(0, -3), require(`./${fileName}`)(config, services) ])
		.fromPairs()
		.value();
};
