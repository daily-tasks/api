const { pipe } = require('../../utils/lodash');
const { unsequelize } = require('../../utils/sequelize');

module.exports = (config, { database: { models: { Capacity, User, Work } } }) => {
	async function list() {
		return User.findAll();
	}

	async function get(id) {
		return User.findOne({
			where: { id },
			include: [ {
				model: Work,
				as: 'works',
				include: [ 'task', 'reward' ],
			} ],
		});
	}

	async function create(data, householdId) {
		// TODO validate datas
		return User.create({
			...data,
			...householdId && { householdId },
		});
	}

	async function acceptTask(userId, taskId, data) {
		// TODO validate datas
		await Capacity.create({ userId, taskId, ...data });
		return await get(userId);
	}

	async function acceptReward(userId, rewardId, data) {
		// TODO validate datas
		await Capacity.create({ userId, rewardId, ...data });
		return await get(userId);
	}

	return {
		acceptTask: pipe(acceptTask, unsequelize),
		acceptReward: pipe(acceptReward, unsequelize),
		create: pipe(create, unsequelize),
		get: pipe(get, unsequelize),
		list: pipe(list, unsequelize),
	};
};
