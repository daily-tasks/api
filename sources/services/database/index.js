const fs = require('fs');
const _ = require('lodash');
const path = require('path');
const { Sequelize } = require('sequelize');

module.exports = async (config) => {
	const { dialect, host, port, name, password, user } = config;
	const sequelize = new Sequelize(name, user, password, {
		dialect,
		host,
		port,
		logging: false,
	});

	const entitiesPath = path.resolve(__dirname, './entities');
	_([ ...fs.readdirSync(entitiesPath), ..._.map(fs.readdirSync(`${entitiesPath}/joins`), (fileName) => `joins/${fileName}`) ])
		.filter((fileName) => fileName.endsWith('.js'))
		.map((fileName) => require(`./entities/${fileName}`)(_.get(config, `entities.${fileName}`)))
		.forEach((entity) => entity.registerTo(sequelize))
		.filter((entity) => typeof entity.registerAssociations === 'function')
		.forEach((entity) => entity.registerAssociations(sequelize));

	await sequelize.sync();
	return sequelize;
};
