const { DataTypes } = require('sequelize');

module.exports = () => ({
	registerTo(sequelize) {
		sequelize.define('Task', {
			label: {
				type: DataTypes.STRING,
				allowNull: false,
			},
		}, {
			tableName: 'tasks',
		});
	},
	registerAssociations({ models: { Capacity, Household, Task, User, Work } }) {
		Task.belongsTo(Household, { as: 'household', foreignKey: 'householdId' });
		Task.belongsToMany(User, { as: 'doableBy', through: Capacity, foreignKey: 'taskId', otherKey: 'userId' });
		Task.hasMany(Work, { as: 'works', foreignKey: 'taskId' });
	},
});
