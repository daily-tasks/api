const _ = require('lodash');
const { DataTypes } = require('sequelize');

module.exports = () => ({
	registerTo(sequelize) {
		sequelize.define('User', {
			label: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			power: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			tasks: {
				type: DataTypes.VIRTUAL,
				get() {
					return _(this.getDataValue('works')).filter((work) => !work.complete).map('task').value();
				},
			},
			rewards: {
				type: DataTypes.VIRTUAL,
				get() {
					return _(this.getDataValue('works')).filter((work) => work.complete && !work.spent).map('reward').value();
				},
			},
		}, {
			tableName: 'users',
		});
	},
	registerAssociations({ models: { Capacity, Household, Reward, Task, User, Work } }) {
		User.belongsTo(Household, { as: 'household', foreignKey: 'householdId' });
		User.belongsToMany(Reward, { as: 'possibleRewards', through: Capacity, foreignKey: 'userId', otherKey: 'rewardId' });
		// User.belongsToMany(Reward, { as: 'rewards', through: Work, foreignKey: 'userId', otherKey: 'rewardId' });
		User.belongsToMany(Task, { as: 'possibleTasks', through: Capacity, foreignKey: 'userId', otherKey: 'taskId' });
		// User.belongsToMany(Task, { as: 'tasks', through: Work, foreignKey: 'userId', otherKey: 'taskId' });
		User.hasMany(Work, { as: 'works', foreignKey: 'userId' });
	},
});
