const { DataTypes } = require('sequelize');

module.exports = () => ({
	registerTo(sequelize) {
		sequelize.define('Capacity', {
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
			userId: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			taskId: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			rewardId: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			value: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
		}, {
			tableName: 'capacites',
		});
	},
});
