const { DataTypes } = require('sequelize');

module.exports = () => ({
	registerTo(sequelize) {
		sequelize.define('Work', {
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
			complete: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: false,
			},
			spent: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: false,
			},
		}, {
			tableName: 'works',
		});
	},
	registerAssociations({ models: { Reward, Task, User, Work } }) {
		Work.belongsTo(Reward, { as: 'reward', foreignKey: 'rewardId' }); // throug
		Work.belongsTo(Task, { as: 'task', foreignKey: 'taskId' });
		Work.belongsTo(User, { as: 'user', foreignKey: 'userId' });
	},
});
