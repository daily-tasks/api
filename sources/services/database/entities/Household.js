const { DataTypes } = require('sequelize');

module.exports = () => ({
	registerTo(sequelize) {
		sequelize.define('Household', {
			label: {
				type: DataTypes.STRING,
				allowNull: false,
			},
		}, {
			tableName: 'households',
		});
	},
	registerAssociations({ models: { Household, Reward, Task, User } }) {
		Household.hasMany(User, { as: 'inhabitants', foreignKey: 'householdId' });
		Household.hasMany(Task, { as: 'tasks', foreignKey: 'householdId' });
		Household.hasMany(Reward, { as: 'rewards', foreignKey: 'householdId' });
	},
});
