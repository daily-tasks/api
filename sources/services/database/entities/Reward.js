const { DataTypes } = require('sequelize');

module.exports = () => ({
	registerTo(sequelize) {
		sequelize.define('Reward', {
			label: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			value: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
		}, {
			tableName: 'rewards',
		});
	},
	registerAssociations({ models: { Capacity, Household, Reward, User, Work } }) {
		Reward.belongsTo(Household, { as: 'household', foreignKey: 'householdId' });
		Reward.belongsToMany(User, { as: 'admissibleBy', through: Capacity, foreignKey: 'rewardId', otherKey: 'userId' });
		Reward.hasMany(Work, { as: 'works', foreignKey: 'taskId' });
	},
});
