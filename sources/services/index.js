const _ = require('lodash');

module.exports = async (config) => {
	const services = {};

	_.set(services, 'database', await require('./database')(config.database, services));
	_.set(services, 'controllers', await require('./controllers')(config, services));

	return services;
};
