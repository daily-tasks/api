(async () => {
	require('./utils/errors');

	const config = await require('./utils/configuration')();

	const services = await require('./services')(config);

	await require('./server')(config, services);
})().catch((error) => {
	console.error('Fatal Error', error);
});
