const fs = require('fs');
const path = require('path');

module.exports = () => {
	if (process.env.VAULT_ADDR) {
		console.log('Fetching vault configuration');
		return require('./vault')();
	}

	const fileLoc = path.resolve(__dirname, '../../../.config.js');
	console.log('Looking of file configuration', fileLoc);
	if (fs.existsSync(fileLoc)) {
		console.log('Fetching file configuration');
		return require(fileLoc);
	}

	return {};
};
