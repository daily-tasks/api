const _ = require('lodash');
const Bluebird = require('bluebird');

const Vault = require('node-vault');

const VAULT_CONFIG = {
	endpoint: process.env.VAULT_ADDR,
	namespace: process.env.VAULT_NAMESPACE,
	credentials: {
		role_id: process.env.VAULT_CR3D3N7I4L5_ROLE_ID,
		secret_id: process.env.VAULT_CR3D3N7I4L5_53CR37_ID,
	},
	paths: process.env.VAULT_PATHS.split('#'),
};

module.exports = async () => {
	console.log(VAULT_CONFIG);
	const vaultClient = Vault({
		endpoint: VAULT_CONFIG.endpoint,
		requestOptions: {
			headers: {
				'X-Vault-Namespace': VAULT_CONFIG.namespace,
			},
		},
	});
	await vaultClient.approleLogin(VAULT_CONFIG.credentials);

	// TODO Reload config each ...
	const records = await Bluebird.map(VAULT_CONFIG.paths, (path) => vaultClient.read(`secret/data/${path}`));
	return records.reduce((acc, record) => _.merge(acc, record.data.data), {});
};
