const _ = require('lodash');
const { Model } = require('sequelize');

function unsequelize(value) {
	if (value instanceof Model) {
		return value.get({ plain: true });
	}
	if (_.isArray(value)) {
		return value.map(unsequelize);
	}
	return value;
}

module.exports = {
	unsequelize,
};
