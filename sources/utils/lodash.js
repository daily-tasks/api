const _ = require('lodash');

module.exports = {
	picker: (arg) => (_.isArray(arg) ? (obj) => _.pick(obj, arg) : (obj) => _.get(obj, arg)),
	pipe: (transform, handler) => async (...args) => handler(await transform(...args)),
};
