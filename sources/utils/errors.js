const { default: Errors, Eratum, registerError } = require('eratum');

Eratum.isStackEnabled = false;

registerError('unauthorized', 'Access denied to <%= resource %>', [ 'resource' ]);

module.exports = Errors;
