const _ = require('lodash');
const express = require('express');
const { throwIf } = require('@plokkke/toolbox');
const { Eratum } = require('eratum');

const Errors = require('./errors');

function rejectable(handler) {
	if (typeof handler !== 'function') {
		return handler;
	}

	if (handler.length > 3) {
		throw Errors.programingFault({ reason: 'handler must have 3 parameter at most' });
	}

	return async (req, res, next) => {
		try {
			if (handler.length === 3) {
				return handler(req, res, next);
			}
			const promise = handler(req, res);
			if (promise instanceof Promise) {
				await promise;
			}
			return next();
		} catch (error) {
			return next(error);
		}
	};
}

async function isAuthorized(guards, authentication, context) {
	if (_.isBoolean(guards)) {
		return guards && authentication;
	}
	const type = authentication ? authentication.type : 'ANONYMOUS';
	if (_.isArray(guards)) {
		return guards.includes(type);
	}
	if (_.isObject(guards)) {
		const authorizer = guards[type];
		return typeof authorizer === 'function' ? await authorizer(authentication.entity, context) : authorizer;
	}
	throw Errors.programingFault({
		reason: 'Unhandled guard type',
		cause: Errors.invalidType({ name: 'guard', actualType: typeof guard, expectedType: 'boolean | object' }),
	});
}

async function assertAuthorization(resource, guards, authentication, context) {
	throwIf(guards && !await isAuthorized(guards, authentication, context), () => Errors.unauthorized({ resource }));
}

function registerRoute(router, route) {
	router[route.method](route.path, ...(route.prerequisites || []), rejectable(async (req, res) => {
		const context = route.contexter && await route.contexter(req);
		await assertAuthorization(req.originalUrl, route.guard, req.authentication, context);
		return res.send(await route.handler(context, req.authentication));
	}, route.id || route.path));
	return router;
}

function registerRoutes(router, routes) {
	_(routes).filter('path')
		.each((route) => {
			if (route.method) {
				registerRoute(router, route);
			} else if (route.routes) {
				router.use(route.path, registerRoutes(express.Router(), route.routes));
			}
		});
	return router;
}

function registerHandlers(app, handlers) {
	_.each(handlers, (handler) => app.use(rejectable(handler)));
}

function errorHandler(cause, req, res, next) {
	if (!cause || res.headersSent) {
		return next();
	}
	if (!(cause instanceof Eratum)) {
		console.log('Error(not Eratum)', typeof cause === 'object' ? JSON.stringify(cause, null, '\t') : cause);
		return res.status(500).send(Errors.programingFault({ cause, reason: 'You found an unhandled case' }).get());
	}
	console.log('Error', JSON.stringify(cause.get(), null, '\t'));
	return res.status(400).send(cause.get());
}

module.exports = {
	errorHandler,
	registerHandlers,
	registerRoutes,
};
